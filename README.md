# Настройка samba сервера и клиента внутри докер контейнеров на виртуальных машинах

## Настройка сетевых адапетеров
Потребуется два физических компьютера с установленными виртуалками на debian 11.
Видеоинструкция по установке доступна [здесь](https://youtu.be/wwflsXqMmOQ)
На одном компьютере будет установлен samba сервер внутри докера (далее - сервер), другой компьютер будет играть роль клиента (далее - клиент). 

Физические компьютеры подключаем друг к другу витой парой. Затем необходимо настроить сетевые адаптеры, сделать это можно через:

**Панель управления -> Сеть и Интернет -> Сетевые подключения**

Начинаем с сервера. При помощи отключения/подключения кабеля можем определить какое именно подключение будем настраивать. Сами настройки доступны через **ПКМ -> Свойства**, посмотреть их можно под катом ниже. Неоходимо задать IP-адрес (192.168.2.101) и маску подсети (255.255.255.0). Также необходимо запонить название подключения ("подключение через"), позже оно пригодится.
<details>
<summary>Настройка сетевого адаптера сервера</summary>
![изображение.png](img/set_adapt.png)
</details>

Далее необходимо произвести настройку клиентского компьютера, отличие от сервера заключается лишь в IP-адресе (192.168.2.101), остальные шаги аналогичны.
<details>
<summary>Настройка сетевого адаптера клиента</summary>
![изображение.png](img/set_adapt2.png)
</details>

## Настройка виртуальной машины
Переходим в настройки сети виртуальной машины на серверном компе, в первом адаптере необходимо пробросить порты samba (137, 138, 139, 445)
Во втором адаптере необходимо пробросить сетевой адаптер, который мы настраивали на предыдущем шаге (вспоминаем его название)
<details>
<summary>Настройка сети VM сервера</summary>
![изображение.png](img/ports.png)
![изображение.png](img/bridge.png)
</details>

Заходим внутрь виртуальной машины, сетевой адаптер уже должен был появиться в ней, но сейчас у него вероянтнее всего нет никаких опознавательных знаков.
Проверить это можно с помощью команды
``` bash
ifconfig -a
``` 

Если команда не найдена предварительно делаем
``` bash
sudo apt-get -y update && sudo apt-get -y install net-tools
``` 

Затем запускаем через 
``` bash
/sbin/ifconfig -a
```

При помощи перезапуска виртуальной машины с проброшенным/отключеным в ней сетевым адаптером и ifconfig определяем какой именно индикатор внутри виртуальной машины характеризует нужный нам сетевой адаптер. Допустим у меня это адаптер с индикатором **enp0s8**

Далее необходимо задать ему IP-адрес (192.168.2.102) и вручную перевести его в статус UP
``` bash
sudo /sbin/ifconfig enp0s8 192.168.2.102 && sudo /sbin/ifconfig enp0s8 up
```

Дополнительно настраивать виртуальную машину клиента не требуется, разве что можно пробросить 22 порт для более удобного подключение через PuTTY.
После этих манипуляций мы должны проверить, что у нас есть возможность пинговать 192.168.2.102 изнутри виртуальной машины запущенной на клиенте.
``` bash
ping 192.168.2.102
```

## Установка Docker
Установить необходимо на обе виртуальные машины (клиент и сервер)
Под катом дана инструкция взятая [тут](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-ru)
<details>
<summary>Подробнее...</summary>

Пакет установки Docker, доступный в официальном репозитории Ubuntu, может содержать не самую последнюю версию. Чтобы точно использовать самую актуальную версию, мы будем устанавливать Docker из официального репозитория Docker. Для этого мы добавим новый источник пакета, ключ GPG от Docker, чтобы гарантировать загрузку рабочих файлов, а затем установим пакет.

Первым делом обновите существующий список пакетов:
``` bash
sudo apt update
```
Затем установите несколько необходимых пакетов, которые позволяют apt использовать пакеты через HTTPS:
``` bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```
Добавьте ключ GPG для официального репозитория Docker в вашу систему:
``` bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
Добавьте репозиторий Docker в источники APT:
``` bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
```
Потом обновите базу данных пакетов и добавьте в нее пакеты Docker из недавно добавленного репозитория:
``` bash
sudo apt update
```
Убедитесь, что установка будет выполняться из репозитория Docker, а не из репозитория Ubuntu по умолчанию:
``` bash
apt-cache policy docker-ce
```
Вы должны получить следующий вывод, хотя номер версии Docker может отличаться:
``` bash
docker-ce:
    Installed: (none)
    Candidate: 5:19.03.9~3-0~ubuntu-focal
    Version table:
        5:19.03.9~3-0~ubuntu-focal 500
            500 https://download.docker.com/linux/ubuntu focal/stable amd64 Packages
```
Обратите внимание, что docker-ce не установлен, но является кандидатом на установку из репозитория Docker для Ubuntu 20.04 (версия focal).

Установите Docker:
``` bash
sudo apt install docker-ce
```
Docker должен быть установлен, демон-процесс запущен, а для процесса активирован запуск при загрузке. Проверьте, что он запущен:
``` bash
sudo systemctl status docker
```
Вывод должен выглядеть примерно следующим образом, указывая, что служба активна и запущена:
``` bash
Output
● docker.service - Docker Application Container Engine
    Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
    Active: active (running) since Tue 2020-05-19 17:00:41 UTC; 17s ago
TriggeredBy: ● docker.socket
     Docs: https://docs.docker.com
 Main PID: 24321 (dockerd)
    Tasks: 8
Memory: 46.4M
CGroup: /system.slice/docker.service
         └─24321 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```
</details>

## Установка Docker Compose
Установить необходимо на обе виртуальные машины (клиент и сервер)
Под катом инструкция взятая [отсюда](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04-ru)

<details>
<summary>Подробнее...</summary>

Следующая команда загружает версию 1.26.0 и сохраняет исполняемый файл в каталоге /usr/local/bin/docker-compose, в результате чего данное программное обеспечение будет глобально доступно под именем docker-compose:
``` bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
Затем необходимо задать правильные разрешения, чтобы сделать команду docker-compose исполняемой:
``` bash
sudo chmod +x /usr/local/bin/docker-compose
```
Чтобы проверить успешность установки, запустите следующую команду:
``` bash
docker-compose --version
```
Вывод будет выглядеть следующим образом:
``` bash
Output
docker-compose version 1.26.0, build 8a1c60f6
```
</details>

## Поднимаем докер контейнер на сервере автоматически
Dockerfile:
``` bash
FROM debian:latest
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y samba samba-client
RUN mkdir -p /samba/public
RUN mkdir -p /samba/private
RUN mkdir -p /samba/password
COPY ./smb.conf /etc/samba
COPY run.sh /scripts/run.sh
COPY .env /scripts/.env
RUN ["chmod", "+x", "/scripts/run.sh"]
ENTRYPOINT ["/scripts/run.sh"]
```
docker-compose.yml:
``` bash
version: "3.8"

services:
    samba:
        build: .
        container_name: samba
        tty: true
        network_mode: 'host'
        volumes:
            - ./public:/samba/public
            - ./private:/samba/private
            - ./password:/samba/password
        # Restart the container if it stops, except that when the container is stopped (manually or otherwise), it is not restarted even after Docker daemon restarts.
        #restart: unless-stopped
```
В той же директории что и yml и Dockerfile кладем скрипт run.sh:
``` bash
#!/bin/bash
export $(grep -v '^#' /scripts/.env | xargs)
adduser --system shareuser
chown -R shareuser /samba/public
adduser --system passworduser
echo -ne "$passworduser\n$passworduser\n" | smbpasswd -a -s passworduser
chown -R passworduser /samba/password
service smbd restart
bash
```
Cоздаем .env:
``` bash
passworduser=123
```
Создаем папки (volumes):
``` bash
mkdir public && mkdir private && mkdir password
```
А также кладем рядом smb.conf
``` bash
[global]
workgroup = WORKGROUP
security = user
map to guest = bad user
wins support = no
dns proxy = no

[public]
path = /samba/public
public = yes
force user = shareuser
browsable = yes
writable = yes

[private]
path = /samba/private
public = yes
browsable = yes
read only = yes

[password]
path = /samba/password
public = yes
browsable = yes
write list = passworduser
```
Поднимаем все это добро через
``` bash
sudo docker-compose build && sudo docker-compose up -d
```

## Поднимаем докер контейнер на сервере руками
Создаем в удобном для себя месте папку, из которой будет разворачивать контейнер
``` bash
mkdir samba && cd samba/
```
Внутри создаем docker-compose.yaml
``` bash
vim docker-compose.yaml
```
Внутри прописываем следующее содержимое
``` bash
version: "3.8"
services:
    sambaserv:
        image: debian:latest
        container_name: sambaserv
        tty:true
        network_mode: 'host'
```
Стартуем
``` bash
sudo docker-compose up -d
```
Заходим внутрь (заменяем contId на свой)
``` bash
sudo docker exec -it contId bash
```
Обновляемся
``` bash
apt-get update
```
Устанавливаем пакеты
``` bash
apt-get install -y samba samba-client
```
Создаем директорию R - все, W - все
``` bash
mkdir -p /samba/public
```
Создаем директорию R - все, W - запрещен
``` bash
mkdir -p /samba/private
```
Создаем директорию R - все, W - запрещен (если монтирование происходило с паролем, тогда W разрешен) 
``` bash
mkdir -p /samba/password
```
Редактируем конфиги самбы
``` bash
vim etc/samba/smb.conf
```
Меняем содержимое на:
``` bash
[global]
workgroup = WORKGROUP
security = user
map to guest = bad user
wins support = no
dns proxy = no

[public]
path = /samba/public
public = yes
force user = shareuser
browsable = yes
writable = yes

[private]
path = /samba/private
public = yes
browsable = yes
read only = yes 

[password]
path = /samba/password
public = yes
browsable = yes
write list = passworduser
```
Создаем пользователя shareuser (Далее будем форсить к нему всех, кто маунтит что-то, что подразумевается публичным)
``` bash
adduser --system shareuser
```
Выдаем ему права на папку public
``` bash
chown -R shareuser /samba/public
```
Создаем пользователя passworduser (Далее будем маунтить через него все то, что подразумевается приватным)
``` bash
adduser --system passworduser
```
Выдаем ему права на папку
``` bash
chown -R passworduser /samba/password
```
Даем ему пароль
``` bash
smbpasswd -a passworduser
```
Рестартим самбу
``` bash
service smbd restart
```
На этом с сервером закончили

## Поднимаем докер контейнер на клиенте автоматически
docker-compose.yml:
``` bash
version: "3.8"

services:
    samba:
        build: .
        container_name: samba
        tty: true
        network_mode: 'host'
        privileged: true

```
Dockerfile:
``` bash
FROM debian:latest
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y samba-client
RUN apt-get install -y cifs-utils
RUN mkdir -p /mnt/publicmnt
RUN mkdir -p /mnt/privatemnt
RUN mkdir -p /mnt/passwordmnt
COPY mount.sh /scripts/mount.sh
RUN ["chmod", "+x", "/scripts/mount.sh"]
ENTRYPOINT ["/scripts/mount.sh"]
```
mount.sh:
``` bash
#!/bin/bash
mount.cifs //192.168.2.102/public /mnt/publicmnt -o guest
mount.cifs //192.168.2.102/private /mnt/privatemnt -o guest
mount.cifs //192.168.2.102/password /mnt/passwordmnt -o guest
bash
```
Поднимаем все это добро через
``` bash
sudo docker-compose build && sudo docker-compose up -d
```
## Поднимаем докер контейнер на клиенте руками
Создаем в удобном для себя месте папку, из которой будет разворачивать контейнер
``` bash
mkdir samba && cd samba/
```
Внутри создаем docker-compose.yaml
``` bash
vim docker-compose.yaml
```
Внутри прописываем следующее содержимое
``` bash
version: "3.8"
services:
    sambaclient:
        image: debian:latest
        container_name: sambaclient
        tty:true
        network_mode: 'host'
        privileged: true
```
Стартуем
``` bash
docker-compose up -d
```
Заходим внутрь (заменяем contId на свой)
``` bash
sudo docker exec -it contId bash
```
Обновляемся
``` bash
apt-get update
```
Устанавливаем пакеты
``` bash
apt-get install -y samba-client
```
Еще устанавливаем такую штуку
``` bash
apt-get install cifs-utils
```
Теперь можем маунтить папки, для этого сначала создаем директории для них
``` bash
mkdir -p /mnt/publicmnt
mkdir -p /mnt/privatemnt
mkdir -p /mnt/passwordmnt
```
Монтирование публичной папки без пароля
``` bash
mount.cifs //192.168.2.102/public /mnt/publicmnt -o guest
```
Монтирование read-only папки без пароля
``` bash
mount.cifs //192.168.2.102/private /mnt/privatemnt -o guest
```
Монтирование папки с разрешением на запись (спросит пароль при монтировании)
``` bash
mount.cifs //192.168.2.102/password /mnt/passwordmnt -o user=passworduser
```
Монтирование той же самой папки без разрешения на запись
``` bash
mount.cifs //192.168.2.102/password /mnt/passwordmnt -o guest
```
## Использование ярлыков
Создаем в удобном для себя месте следующую струтуру из директорий

<details>
<summary>Структура</summary>
![изображение.png](img/tree.png)
</details>

Выдаем shareuser права на папки R - все, W - все
``` bash
chown -R shareuser /samba/General
chown -R shareuser /samba/General/Users
```
Создаем пользователя scaner
``` bash
adduser --system scaner
```
Выдаем ему права на папку Scaner
``` bash
chown -R scaner /samba/Scaner
```
Даем ему пароль
``` bash
smbpasswd -a scaner
```
Создаем пользователя userfirst
``` bash
adduser --system userfirst
```
Выдаем ему права на папку User1
``` bash
chown -R userfirst /samba/User1
```
Даем ему пароль
``` bash
smbpasswd -a userfirst
```
Редактируем конфиги самбы
``` bash
vim etc/samba/smb.conf
```
Меняем содержимое на:
``` bash
[global]
workgroup = WORKGROUP
security = user
map to guest = bad user
wins support = no
dns proxy = no
allow insecure wide links = yes

[Scaner]
path = /samba/Scaner
public = yes
browsable = yes
write list = scaner

[User1]
path = /samba/User1
public = no
browsable = yes
write list = userfirst

[General]
path = /samba/General
public = yes
force user = shareuser
browsable = yes
writable = yes

[Users]
path = /samba/General/Users
public = yes
force user = shareuser
browsable = yes
writable = yes
follow symlinks = yes
wide links = yes
```
Рестартим самбу
``` bash
service smbd restart
```
Идем в /General/Users
``` bash
cd samba/General/Users
```
Создаем ярлык
``` bash
ln -s /samba/Scaner ./
```
Готово, можем монтировать эти папки на клиентах

## Автоматическое монтирование при запуске/перезапуске
Нам необходимо отредактировать системный файл **/etc/fstab** сделать это можно только от рута, поэтому сначала переходим в него
``` bash
sudo -i
```
Затем добавляем в **/etc/fstab** последней строкой (при этом меняем ip, username и путь до папки, в которую маунтим на нужный, но помним, что маунтить виндозные папки стоит под юзером **user**)
``` bash
//192.168.161.96/.sessions /mnt cifs username=user,x-systemd.automount 0 0
```
Готово, можем перезагружаться и проверять, что все автоматически смонтировалось.

# Инструкция Лехи

#SAMBA

## Docker
Требуется его установить. Инструкция и команды [тут](../../utils/docker/readme.md)

## Настройка папок
```bash 
sudo mkdir -p /home/services/samba && cd "$_"
sudo mkdir -p data
```
### Подготовка контейнера
`sudo nano Dockerfile`
```bash
FROM debian:latest
USER root
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y samba samba-client
# RUN apt-get install -y samba

COPY smb.conf /etc/samba
COPY run.sh /usr/share/samba/run.sh
COPY .env /usr/share/samba/.env
RUN chmod +x /usr/share/samba/run.sh
ENTRYPOINT ["/usr/share/samba/run.sh"]

```
---
`sudo nano docker-compose.yml`
```yaml
version: "3.8"

services:
    samba:
        build: .
        container_name: samba
#        ports:
#       - 445:445
        tty: true
        network_mode: 'host'
        volumes:
            - ./data:/usr/share/samba/data/
        # Restart the container if it stops, except that when the container is stopped (manually or otherwise), it is not restarted even after Docker daemon restarts.
        #restart: unless-stopped
```
---
`sudo nano run.sh`
```bash
#!/bin/bash
mkdir -p /usr/share/samba/data/{y,z,users/koshi8bit}
adduser --system guest
chown -R guest /usr/share/samba/data/y

export $(grep -v '^#' /usr/share/samba/.env | xargs)
adduser --system koshi8bit
echo -ne "$pass_koshi8bit\n$pass_koshi8bit\n" | smbpasswd -a -s koshi8bit
chown -R koshi8bit /usr/share/samba/data/users/koshi8bit
service smbd restart
rm /usr/share/samba/.env
bash
```
---
`sudo nano .env`
```
pass_koshi8bit=pass
```
---

`sudo nano smb.conf`
```bash
[global]
workgroup = WORKGROUP
security = user
map to guest = bad user
wins support = no
dns proxy = no

[y]
path = /usr/share/samba/data/y
public = yes
force user = guest
browsable = yes
writable = yes

[z]
path = /usr/share/samba/data/z
public = yes
browsable = yes
read only = yes
```
---
```bash
sudo docker-compose -f docker-compose.yml up --detach; echo -e '\a';

docker exec -it samba /bin/bash
ls -la /usr/share/samba/

docker build --no-cache -t samba_samba .

docker stop samba && docker rm samba && docker image rm samba_samba
```

