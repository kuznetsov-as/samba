#!/bin/bash
export $(grep -v '^#' /scripts/.env | xargs)
adduser --system shareuser
chown -R shareuser /samba/public
adduser --system passworduser
echo -ne "$passworduser\n$passworduser\n" | smbpasswd -a -s passworduser
chown -R passworduser /samba/password
service smbd restart
bash
